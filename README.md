# InfluxData

InfluxData TICK stack

## Start

### Prerequisites
A working Docker Swarm cluster. If you want to deploy on single nodes, init the Swarm cluster on each node as well with `docker swarm init`.

A record in your local DNS whith the name `influxdb` pointing to the IP of the node were you're planning to start the TICK stack.

### TICK node
Create volumes directories as per `docker-compose-influxdata.yml` file and then start the stack with the following command:

```shell
export HOST=$(hostname)
docker stack deploy -c docker-compose-influxdata.yml influxdata
```

or execute the provided command `bin/start-influxdata`

### Telegraf nodes
Modify the environment variable `HOST` in `docker-compose-telegraf.yml` to match the local hostname and start the stack with the following command:

```shell
export HOST=$(hostname)
docker stack deploy -c docker-compose-telegraf.yml telegraf
```

or execute the provided command `bin/start-telegraf`

## Further reading
Additional information can be found on the [InfluxData web site](https://www.influxdata.com).
